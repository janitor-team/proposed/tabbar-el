(define-package "tabbar"
  "20160524" "Display a tab bar in the header line"
  'nil
  :keywords '("convenience")
  :url "https://github.com/dholm/tabbar")
